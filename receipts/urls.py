from django.urls import path
from receipts.views import create_account, create_category, create_receipt, show_accounts, show_categories, show_receipt

urlpatterns = [
    path("", show_receipt, name="home"),  # name="home" is used by django to reference from other files
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_categories, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", show_accounts, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
