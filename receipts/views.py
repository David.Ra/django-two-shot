from multiprocessing import context
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from receipts.forms import AccountForm, ReceiptForm, ExpenseCategoryForm
from receipts.models import Account, ExpenseCategory, Receipt


# Shortcut
# Create your views here.
# @login_required(login_url='login') << if you want to define the redirect inline
@login_required
def show_receipt(request):
    context = {"receipt_list": Receipt.objects.filter(purchaser=request.user)}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        receipt_form = ReceiptForm(request.POST)
        if receipt_form.is_valid():
            vendor = receipt_form.cleaned_data["vendor"]
            total = receipt_form.cleaned_data["total"]
            tax = receipt_form.cleaned_data["tax"]
            date = receipt_form.cleaned_data["date"]
            category = receipt_form.cleaned_data["category"]
            account = receipt_form.cleaned_data["account"]
            purchaser = request.user

            Receipt.objects.create(
                vendor=vendor,
                total=total,
                tax=tax,
                date=date,
                category=category,
                account=account,
                purchaser=purchaser,
            )
            return redirect("home")

    receipt_form = ReceiptForm()
    context = {"receipt_form": receipt_form}
    return render(request, "receipts/create_receipt.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        expense_form = ExpenseCategoryForm(request.POST)
        if expense_form.is_valid():
            name = expense_form.cleaned_data["name"]
            owner = request.user

            ExpenseCategory.objects.create(
                name=name,
                owner=owner,
            )
            return redirect("category_list")

    expense_form = ExpenseCategoryForm()
    context = {"expense_form": expense_form}
    return render(request, "receipts/create_category.html", context)

@login_required
def show_categories(request):
    filtered_categories = ExpenseCategory.objects.filter(owner=request.user)
    for category in filtered_categories:
        category.length = len(Receipt.objects.filter(category=category))
    context = {"filtered_categories": filtered_categories}
    return render(request, "receipts/categories_list.html", context)

@login_required
def show_accounts(request):
    filtered_accounts = Account.objects.filter(owner=request.user)
    for account in filtered_accounts:
        account.length = len(Receipt.objects.filter(account=account))
    context = {"filtered_accounts": filtered_accounts}
    return render(request, "receipts/accounts_list.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        account_form = AccountForm(request.POST)
        if account_form.is_valid():
            name = account_form.cleaned_data["name"]
            number = account_form.cleaned_data["number"]
            owner = request.user

            Account.objects.create(
                name=name,
                number=number,
                owner=owner,
            )
            return redirect("account_list")

    account_form = AccountForm()
    context = {"account_form": account_form}
    return render(request, "receipts/create_account.html", context)

# Long method
# def show_receipt(request):
#     print('Check at show_receipt: '+str(request.user.is_authenticated))
#     if request.user.is_authenticated:
#         context = {
#             "receipt_list": Receipt.objects.filter(purchaser=request.user)
#         }
#         return render(request, "receipts/list.html", context)

#     return redirect('login')
