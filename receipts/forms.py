from django.forms import ModelForm
from receipts.models import Account, ExpenseCategory, Receipt


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        exclude = ["purchaser"]

class ExpenseCategoryForm(ModelForm): #Feat 13
    class Meta:
        model = ExpenseCategory
        fields = ["name"]

class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = ["name", "number"]
