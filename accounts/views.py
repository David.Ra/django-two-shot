from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# https://www.domainname.com/accounts/login
def main_login(request): # Feat 8
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            print("Attempted Login:")
            print(f"Username: {username}")
            print(f"Password: {password}")

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                print('User Authenticated')
                login(request, user)
                print('User Logged In')
                print('Check at main_login: '+str(user.is_authenticated))
                return redirect("home")

    form = LoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)

def main_logout(request): #Feat 9
    logout(request)
    return redirect("login")

def main_signup(request):
    if request.method == "POST":
        sign = SignForm(request.POST)
        if sign.is_valid():
            username = sign.cleaned_data['username']
            password = sign.cleaned_data['password']
            password_confirmation = sign.cleaned_data['password_confirmation']

            if password == password_confirmation:
                user = User.objects.create_user(username, None, password)
                login(request, user)
                return redirect("home")
            else:
                sign.add_error('password', "Passwords do not match")
    else:
        sign = SignForm()
        # this is the get request Feat 10
    context = {"sign": sign}
    return render(request, "accounts/signup.html", context)
