from django.urls import path
from accounts.views import main_login, main_logout, main_signup

urlpatterns = [
    path("login/", main_login, name="login"),
    path("signup/", main_signup, name="signup"),
    path("logout/", main_logout, name="logout"),
]
